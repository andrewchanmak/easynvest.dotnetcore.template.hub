# Easynvest.DotnetCore.Template.Hub

Baixe o repositorio e para instalar o template é necessario utilizar os passos abaixo

## Instalar
```
dotnet new -i <caminho do template que vc baixou>
```

## Criar um novo projeto usando o template
```
dotnet new TemplateHub -n <nome do seu projeto>
dotnet new TemplateHub -n Easynvest.MeuNovoProjeto (exemplo)
```

## Desinstalar

### Lista templates instalados a serem apagados
```
dotnet new -u
```

### Desinstalar
```
dotnet new -u <caminho completo do projeto a ser desinstalado>
```

