﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Easynvest.DotnetCore.Template.CrossCutting.Interfaces;
using Easynvest.KingsCross.Bus.Topology;
using Easynvest.KingsCross.Bus.Topology.Contracts;
using Easynvest.KingsCross.RabbitMq.Model;

namespace Easynvest.DotnetCore.Template.CrossCutting.Messaging
{
    [ExcludeFromCodeCoverage]
    public class EndpointFactory : IEndpointFactory
    {
        public IEndPointConfigurator GetEndPoint(MessagingOption messagingOption)
        {
            IExchangeConfiguration exchange = new ExchangeConfiguration(messagingOption.Exchange)
            {
                Type = messagingOption.Type,
                Durable = true,
                AutoDelete = false,
                Arguments = new Dictionary<string, object>()
            };
            IQueueConfiguration queue = new QueueConfiguration(messagingOption.Queue)
            {
                Durable = true,
                Exclusive = false,
                AutoDelete = false,
                ImplementDeadletter = false,
                Arguments = new Dictionary<string, object>()
            };
            return new EndPointConfigurator(exchange, queue, messagingOption.RoutingKey);
        }
    }
}
