﻿using Easynvest.KingsCross.Bus.Topology.Contracts;

namespace Easynvest.DotnetCore.Template.CrossCutting.Interfaces
{
    public interface IEndpointFactory
    {
        IEndPointConfigurator GetEndPoint(MessagingOption messagingOption);
    }
}
