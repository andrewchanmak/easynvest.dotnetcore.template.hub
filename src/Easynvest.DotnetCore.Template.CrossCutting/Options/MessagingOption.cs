﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.DotnetCore.Template.CrossCutting
{
    [ExcludeFromCodeCoverage]
    public class MessagingOption
    {
        public string Exchange { get; set; }
        public string RoutingKey { get; set; }
        public string Type { get; set; }
        public string Queue { get; set; }
    }
}
