﻿using System.Diagnostics.CodeAnalysis;

namespace Easynvest.DotnetCore.Template.CrossCutting
{
    [ExcludeFromCodeCoverage]
    public class RabbitMQConnectionSettingOptions
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
    }
}
