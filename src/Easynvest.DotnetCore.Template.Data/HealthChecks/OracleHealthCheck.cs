﻿using App.Metrics.Health;
using Easynvest.DotnetCore.Template.Domain.Repositories;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.DotnetCore.Template.Data.HealthChecks
{
    public class OracleHealthCheck : HealthCheck
    {
        private readonly IHealthCheckRepository _repository;

        public OracleHealthCheck(IHealthCheckRepository repository)
            : base("oracle_connection_check") => _repository = repository;

        protected override ValueTask<HealthCheckResult> CheckAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                _repository.PingOracle();
                return new ValueTask<HealthCheckResult>(HealthCheckResult.Healthy());
            }
            catch (Exception ex)
            {
                return new ValueTask<HealthCheckResult>(HealthCheckResult.Unhealthy(ex.Message));
            }
        }
    }
}
