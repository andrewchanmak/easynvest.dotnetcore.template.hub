﻿using App.Metrics.Health;
using Easynvest.KingsCross.Bus.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace Easynvest.DotnetCore.Template.Data.HealthChecks
{
    public class RabbitMqHealthCheck : HealthCheck
    {
        private readonly IRabbitMQPersistentConnection _connection;

        public RabbitMqHealthCheck(IRabbitMQPersistentConnection connection) : base("rabbitmq_connection_check")
        {
            _connection = connection;
        }

        protected override ValueTask<HealthCheckResult> CheckAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            using (_connection)
            {
                return _connection.HealthCheck() 
                    ? new ValueTask<HealthCheckResult>(HealthCheckResult.Healthy()) 
                    : new ValueTask<HealthCheckResult>(HealthCheckResult.Unhealthy());
            }
        }
    }
}
