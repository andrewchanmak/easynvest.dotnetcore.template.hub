﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace Easynvest.DotnetCore.Template.Data.Repositories
{
    public abstract class OracleRepository
    {
        protected IConfiguration Configuration { get; }

        protected OracleRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected IDbConnection CreateOracleConnection(string connectionStringName)
            => new OracleConnection(Configuration.GetConnectionString(connectionStringName));
    }
}
