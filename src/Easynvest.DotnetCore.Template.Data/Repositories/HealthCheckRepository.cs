﻿using Dapper;
using Easynvest.DotnetCore.Template.Domain.Repositories;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;

namespace Easynvest.DotnetCore.Template.Data.Repositories
{
    public class HealthCheckRepository : OracleRepository, IHealthCheckRepository
    {
        private IDistributedCache _cache { get; }

        public HealthCheckRepository(IConfiguration configuration, IDistributedCache cache)
            : base(configuration)
        {
            _cache = cache;
        }

        public void PingOracle()
        {
            using (var connection = CreateOracleConnection("Oracle"))
            {
                connection.QuerySingle("SELECT 1 FROM CORRWIN.TCCMOVTO WHERE ROWNUM = 1").ConfigureAwait(false);
            }
        }

        public void PingRedis()
        {
            _cache.GetString("cache_key");
        }
    }
}
