﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO.Compression;

namespace Easynvest.DotnetCore.Template.Api.Extensions
{
    public static class MvcExtensions
    {
        public static IServiceCollection AddCommonMvcServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.IgnoreNullValues = true); 

            services
                .AddHealthCheck()
                .AddVersioning()
                .ConfigureGzip()
                .AddLogging();

            return services;
        }

        private static IServiceCollection ConfigureGzip(this IServiceCollection services)
        {
            return services
                .Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Optimal)
                .AddResponseCompression(options => options.Providers.Add<GzipCompressionProvider>());
        }

        public static IApplicationBuilder UseCommonMvc(this IApplicationBuilder application, IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            return application
                .UseRouting()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                });

        }
    }
}
