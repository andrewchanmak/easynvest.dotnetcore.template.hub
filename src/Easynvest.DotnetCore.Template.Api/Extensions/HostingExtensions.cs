﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Easynvest.DotnetCore.Template.Api.Extensions
{
    public static class HostingExtensions
    {
        public static bool IsHomolog(this IWebHostEnvironment environment) 
            => environment.IsEnvironment("Homolog");
    }
}
