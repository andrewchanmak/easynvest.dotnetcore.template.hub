﻿using App.Metrics.Health;
using Easynvest.DotnetCore.Template.Data.HealthChecks;
using Easynvest.DotnetCore.Template.Domain.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Easynvest.DotnetCore.Template.Api.Extensions
{
    public static class HealthCheckExtensions
    {
        public static IServiceCollection AddHealthCheck(this IServiceCollection services)
        {
            services
                .AddHealth(builder =>
                {
                    var provider = services.BuildServiceProvider();
                    var healthCheckRepository = provider.GetServices<IHealthCheckRepository>().ElementAt(0);

                    builder.OutputHealth.AsJson();

                    builder.HealthChecks.AddCheck(new OracleHealthCheck(healthCheckRepository));
                    builder.HealthChecks.AddCheck(new RedisHealthCheck(healthCheckRepository));
                });

            services.AddHealthEndpoints();

            return services;
        }
    }
}
