﻿using System.Collections.Generic;
using System.Globalization;
using Easynvest.DotnetCore.Template.Api.Extensions;
using Easynvest.DotnetCore.Template.Api.Helpers;
using Easynvest.DotnetCore.Template.IoC;
using Easynvest.KingsCross.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Easynvest.DotnetCore.Template.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services
                .AddOptions(Configuration)
                .AddKingsCross(Configuration)
                .AddDistributedRedisCache(options => options.Configuration = Configuration["ConnectionStrings:Redis"])
                .AddAuthenticatedUser()
                .AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>()
                .AddSwagger()
                .AddJwtAuthentication(Configuration)
                .AddLogging()                
                .AddApplicationServices(Configuration)
                .AddCommonMvcServices(Configuration);

        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory, IWebHostEnvironment env, IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            var cultureInfo = new CultureInfo("pt-BR");
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

            if (env.IsDevelopment() || env.IsHomolog())
            {
                app.UseDeveloperExceptionPage();
            }

            if (!env.IsProduction())
                app.UseSwagger(c =>
                {
                    c.PreSerializeFilters.Add((apiDoc, httpReq) =>
                    {
                        apiDoc.Servers = new List<OpenApiServer>
                               {
                                   new OpenApiServer {Url = $"{httpReq.Scheme}://{httpReq.Host.Value}/", Description = "Development"},
                                   new OpenApiServer {Url = $"{httpReq.Scheme}://{httpReq.Host.Value}/template", Description = "Kubernetes"}
                               };
                    });
                }
                   )
                   .UseSwaggerUI(c => c.SwaggerEndpoint("v1/swagger.json", "v1"));

            app
                .UseHealthAllEndpoints()
                .UseCommonMvc(apiVersionDescriptionProvider);

        }
    }

}
