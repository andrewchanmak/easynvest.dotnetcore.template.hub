﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Easynvest.DotnetCore.Template.Api.Controllers
{
    [ApiController]
    public abstract class BaseController : Controller
    {
        private readonly IMediator _mediator;

        protected BaseController(IMediator mediator)
            => _mediator = mediator;

        protected async Task<T> Execute<T>(IRequest<T> request)
            => await _mediator.Send(request).ConfigureAwait(false);
    }
}
