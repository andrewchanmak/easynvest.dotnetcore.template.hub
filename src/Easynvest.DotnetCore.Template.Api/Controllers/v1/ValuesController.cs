﻿using Easynvest.DotnetCore.Application.Query.Queries;
using Easynvest.Logger;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Easynvest.DotnetCore.Template.Api.Controllers.v1
{
    [ApiVersion("1.0"), Route("v{api-version:apiVersion}/values")]
    public class ValuesController : BaseController
    {
        private readonly ILogger<ValuesController> _logger;

        public ValuesController(ILogger<ValuesController> logger, IMediator mediator)
            : base(mediator)
        {
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<string>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation($"[Easynvest.DotnetCore.Template][ValuesController][Get] - Get");
            //testando o easynvest.ops, para dar 200 só deixar positivo.
            var response = await Execute(new GetTesteQuery() { Id =  1 });

            if (response.IsFailure)
                return BadRequest(response);

            return Ok(response.Value);
        }

        [HttpPost("{value}")]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(void), (int)HttpStatusCode.InternalServerError)]
        public IActionResult Post([FromRoute] string value)
        {
            _logger.LogInformation($"[Easynvest.DotnetCore.Template][ValuesController][Post] - Post");

            return Ok(value);
        }
    }
}
