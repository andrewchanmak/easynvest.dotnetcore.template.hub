﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Easynvest.DotnetCore.Template.IoC;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Enrichers.AspnetcoreHttpcontext;
using Serilog.Events;
using Serilog.Formatting.Elasticsearch;
using Serilog.Sinks.Elasticsearch;
namespace Easynvest.DotnetCore.Template.Api
{
    [ExcludeFromCodeCoverage]
    public class Program
    {
        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                       .ConfigureWebHostDefaults(webBuilder =>
                       {
                           webBuilder.UseStartup<Startup>()
                                     .UseSerilog(CreateLoggerConfiguration);
                       });
        }

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static void CreateLoggerConfiguration(IServiceProvider provider, WebHostBuilderContext hostingContext,
          LoggerConfiguration loggerConfiguration)
        {
            var assembly = Assembly.GetExecutingAssembly().GetName();

            loggerConfiguration
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.AspNetCore.Mvc", LogEventLevel.Error)
                .Enrich.FromLogContext()
                .Enrich.WithProperty("Assembly", $"{assembly.Name}")
                .Enrich.WithProperty("Version", $"{assembly.Version}")
                .Enrich.WithAspnetcoreHttpcontext(provider, SerilogHttpContextExtension.CustomEnrichLogic)
                .WriteTo.Console(
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {Properties:j}{NewLine}{Exception}",
                    restrictedToMinimumLevel: LogEventLevel.Debug)
                .WriteTo.Elasticsearch(
                    new ElasticsearchSinkOptions(
                        new Uri(hostingContext.Configuration.GetConnectionString("Elasticsearch")))
                    {
                        AutoRegisterTemplate = true,
                        RegisterTemplateFailure = RegisterTemplateRecovery.IndexAnyway,
                        AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv6,
                        IndexFormat = "servers{0:yyyy.MM.dd}",
                        CustomFormatter =
                            new ElasticsearchJsonFormatter(inlineFields: true, renderMessageTemplate: false)
                    }).ReadFrom.Configuration(hostingContext.Configuration);
        }
    }
}
