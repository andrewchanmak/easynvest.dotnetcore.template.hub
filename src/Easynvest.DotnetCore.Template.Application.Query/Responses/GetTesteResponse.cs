﻿
namespace Easynvest.DotnetCore.Application.Query.Responses
{
    public class GetTesteResponse
    {
        public GetTesteResponse(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}
