﻿using Easynvest.DotnetCore.Application.Query.Responses;
using Easynvest.DotnetCore.Template.Domain.Responses;
using MediatR;

namespace Easynvest.DotnetCore.Application.Query.Queries
{
    public class GetTesteQuery : IRequest<Response<GetTesteResponse>>
    {
        public int Id { get; set; }
    }
}
