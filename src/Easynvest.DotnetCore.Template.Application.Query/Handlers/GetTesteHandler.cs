﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Easynvest.DotnetCore.Application.Query.Queries;
using Easynvest.DotnetCore.Application.Query.Responses;
using Easynvest.DotnetCore.Template.Domain.Responses;
using Easynvest.Ops;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Easynvest.DotnetCore.Application.Query.Handlers
{
    public class GetTesteHandler : IRequestHandler<GetTesteQuery, Response<GetTesteResponse>>
    {
        private readonly ILogger<GetTesteHandler> _logger;

        public GetTesteHandler(
            ILogger<GetTesteHandler> logger)
        {
            _logger = logger;
        }

        public async Task<Response<GetTesteResponse>> Handle(GetTesteQuery request, CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogInformation($"Iniciando 'GetTeste");
                if (request.Id < 0)
                {
                    _logger.LogWarning($"deu um problema aqui: {DateTime.Now}");
                    return Response<GetTesteResponse>.Fail(new Error("InvalidParameter", "Identificação do usuário é obrigatória",
                                                   StatusCodes.Status400BadRequest));
                }

                return Response<GetTesteResponse>.Ok(new GetTesteResponse("I'm pickle Rick"));
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Ocorreu um erro.");
                return Response<GetTesteResponse>.Fail(ex.Message);
            }
        }
    }
}
