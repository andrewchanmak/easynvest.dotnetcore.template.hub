﻿using Easynvest.DotnetCore.Template.CrossCutting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.DotnetCore.Template.IoC
{
    public static class ConfigureOptionsContainerEx
    {
        public static IServiceCollection AddOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<EndpointsOptions>(configuration.GetSection("Endpoints"));
            services.Configure<RabbitMQConnectionSettingOptions>(configuration.GetSection("RabbitMQConnectionSetting"));
            services.Configure<MessagingConfigurationOptions>(configuration.GetSection("MessagingConfiguration"));
            return services;
        }
    }
}
