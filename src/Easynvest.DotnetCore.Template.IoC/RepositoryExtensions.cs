﻿using Easynvest.DotnetCore.Template.Data.Repositories;
using Easynvest.DotnetCore.Template.Domain.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.DotnetCore.Template.IoC
{
    internal static class RepositoryExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            return services
                    .AddScoped<IHealthCheckRepository, HealthCheckRepository>();
        }
    }
}
