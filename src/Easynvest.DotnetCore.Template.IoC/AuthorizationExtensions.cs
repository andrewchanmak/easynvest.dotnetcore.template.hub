﻿using System.Diagnostics.CodeAnalysis;
using Easynvest.DotnetCore.Template.CrossCutting.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.DotnetCore.Template.IoC
{
    [ExcludeFromCodeCoverage]
    public static class AuthorizationExtensions
    {
        public static IServiceCollection AddAuthenticatedUser(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<AuthenticatedUser>();

            return services;
        }
    }
}
