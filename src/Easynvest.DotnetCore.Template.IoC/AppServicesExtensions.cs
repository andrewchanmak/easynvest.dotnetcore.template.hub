﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.DotnetCore.Template.IoC
{
    public static class AppServicesExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                    .AddHandlers()
                    .AddRepositories()
                    .AddRabbitMqServices();
        }
    }
}
