﻿using Easynvest.DotnetCore.Template.CrossCutting.Interfaces;
using Easynvest.DotnetCore.Template.CrossCutting.Messaging;
using Easynvest.KingsCross.Bus.Contracts;
using Easynvest.KingsCross.RabbitMq;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.DotnetCore.Template.IoC
{
    public static class RabbitMqExtension
    {
        public static IServiceCollection AddRabbitMqServices(this IServiceCollection services)
        {
            return services
                   .AddSingleton<IKingsCrossServiceBus, KingsCrossServiceBusRabbitMq>()
                   .AddSingleton<IEndpointFactory, EndpointFactory>();
        }
    }
}
