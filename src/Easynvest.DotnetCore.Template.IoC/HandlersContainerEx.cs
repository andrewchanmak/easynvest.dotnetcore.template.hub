﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.DotnetCore.Template.IoC
{
    [ExcludeFromCodeCoverage]
    public static class HandlersContainerEx
    {
        public static IServiceCollection AddHandlers(this IServiceCollection services)
        {     
            const string applicationWriteContextName = "Easynvest.DotnetCore.Template.Application.Command";
            var applicationWriteContext = AppDomain.CurrentDomain.Load(applicationWriteContextName);

            const string applicationReadContextName = "Easynvest.DotnetCore.Template.Application.Query";
            var applicationReadContext = AppDomain.CurrentDomain.Load(applicationReadContextName);

            services.AddMediatR(applicationWriteContext);
            services.AddMediatR(applicationReadContext);

            return services;
        }
    }
}
