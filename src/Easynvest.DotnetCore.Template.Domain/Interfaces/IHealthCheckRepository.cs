﻿namespace Easynvest.DotnetCore.Template.Domain.Repositories
{
    public interface IHealthCheckRepository
    {
        void PingOracle();
        void PingRedis();
    }
}
