﻿using Microsoft.Extensions.Logging;
using NSubstitute;
using System;

namespace Easynvest.DotnetCore.Template.Tests.Extensions
{
    public static class TestExtensions
    {
        public static void CheckLogger(this ILogger logger, int quantity, LogLevel logLevel) => logger.CheckReceivedByQuantity(quantity, logLevel);

        public static void CheckLogger(this ILogger logger, LogLevel logLevel) => logger.CheckReceived(logLevel);

        public static void CheckErrorMessage(this ILogger logger, string message) => logger.CheckMessage(LogLevel.Error, message);

        public static void CheckInformationMessage(this ILogger logger, string message) => logger.CheckMessage(LogLevel.Information, message);

        public static void CheckMessage(this ILogger logger, LogLevel logLevel, string message)
        {
            logger.Received().Log(
               logLevel,
               Arg.Any<EventId>(),
               Arg.Is<object>(o => o.ToString() == message),
               null,
               Arg.Any<Func<object, Exception, string>>());
        }

        public static void CheckReceived(this ILogger logger, LogLevel logLevel)
        {
            logger.Received().Log(
               logLevel,
               Arg.Any<EventId>(),
               Arg.Any<object>(),
               null,
               Arg.Any<Func<object, Exception, string>>());
        }

        public static void CheckReceivedByQuantity(this ILogger logger, int quantity, LogLevel logLevel)
        {
            logger.Received(quantity).Log(
               logLevel,
               Arg.Any<EventId>(),
               Arg.Any<object>(),
               null,
               Arg.Any<Func<object, Exception, string>>());
        }

        public static void CheckNotReceived(this ILogger logger, LogLevel logLevel)
        {
            logger.DidNotReceive().Log(
               logLevel,
               Arg.Any<EventId>(),
               Arg.Any<object>(),
               null,
               Arg.Any<Func<object, Exception, string>>());
        }
    }
}
