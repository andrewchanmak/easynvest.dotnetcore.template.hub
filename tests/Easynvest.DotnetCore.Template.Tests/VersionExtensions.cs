﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;

namespace Easynvest.DotnetCore.Template.ApiConfiguration
{
    public static class VersionExtensions
    {
        public static IServiceCollection AddVersioning(this IServiceCollection services)
        {
            return services
                    .AddVersionedApiExplorer(options =>
                        {
                            options.GroupNameFormat = "'v'VVV";
                            options.SubstituteApiVersionInUrl = true;
                        })
                    .AddApiVersioning(options =>
                        {
                            options.ReportApiVersions = true;
                            options.AssumeDefaultVersionWhenUnspecified = true;
                            options.DefaultApiVersion = new ApiVersion(1, 0);
                            options.ApiVersionReader = new UrlSegmentApiVersionReader();
                        });
        }
    }
}
